package org.aossie.agoraandroid.data.network.responses

import org.aossie.agoraandroid.data.db.model.Ballot

data class Ballots(
  var ballots: List<Ballot>
)